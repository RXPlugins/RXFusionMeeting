//
//  FusionMeeting.h
//  FusionMeeting
//
//  Created by Brown on 2017/10/24.
//  Copyright © 2017年 Brown. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseComponent.h"

@interface FusionMeeting : BaseComponent

//SYNTHESIZE_SINGLETON_FOR_CLASS_HEADER(FusionMeeting);
+ (FusionMeeting *)sharedInstance;

/**
 SVC会议页面
 */
- (UIViewController *)getConferenceViewController;

/**
 SVC会议消息列表页面
 */
- (UIViewController *)getConferenceMessageListViewController;

/**
 接收会议变更消息
 */
- (void)onReceivedConfAlterationMsg:(ECConferenceNotification*)info;

/**
 会议邀请通知
 */
-(void)getInviteWithMsg:(ECConferenceInviteNotification *)inviteInfoMsg;

/**
 获取会议配置信息
 */
- (void)getConferenceAppSetting;

/**
 立即开始会议
 @param members 会议成员
 */
- (void)startConferenceWithMembers:(NSArray *)members;

@end
